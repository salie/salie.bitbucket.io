(function() {
    var GiphyApp = angular.module("GiphyApp", []);

    var GiphyCtrl = function($http) {
        var giphyCtrl = this;

        giphyCtrl.subject = "";
        giphyCtrl.image = "/landing-img.jpg";   

        giphyCtrl.search = function() {
            if (!giphyCtrl.subject || (giphyCtrl.subject.trim().length <= 0))
                return
            console.log("searching %s", giphyCtrl.subject);
            var promise = $http.get("http://api.giphy.com/v1/gifs/random", {
                params: {
                    api_key: "dc6zaTOxFJmzC",
                    tag: giphyCtrl.subject
                }
            });
            promise.then(function(result) {
                giphyCtrl.image = result.data.data.image_url;
            });
        };
    };
    GiphyCtrl.$inject = ["$http"];

    GiphyApp.controller("GiphyCtrl", GiphyCtrl);
})();